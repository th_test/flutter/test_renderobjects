import 'dart:io';
import 'dart:math';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:test_renderobjects/column_rtw.dart';

class CustomBox extends LeafRenderObjectWidget {
  final int flex;
  final Color color;
  final double rotation;
  final VoidCallback onTap;

  CustomBox({this.flex = 0, this.color, this.rotation = 0, this.onTap});

  @override
  RenderObject createRenderObject(BuildContext context) {
    return RenderObjectCustomBox(flex: flex, color: color, rotation: rotation, onTap: onTap);
  }

  // called when customBox is update,
  // and the renderObject is the previous renderObject
  @override
  void updateRenderObject(BuildContext context, covariant RenderObjectCustomBox renderObject) {
    renderObject
      ..flex = flex
      ..rotation = rotation
      ..onTap = onTap
      ..color = color;
  }

}


class RenderObjectCustomBox extends RenderBox {
  final _painter = Paint();

  VoidCallback _onTap;

  set onTap(value) {
    this._onTap = value;
    _tapGestureRecognizer.onTap = _onTap;
  }

  TapGestureRecognizer _tapGestureRecognizer;

  VoidCallback get onTap => _onTap;


  RenderObjectCustomBox({
    int flex,
    Color color,
    double rotation,
    VoidCallback onTap
  }): _flex = flex, _color = color, _rotation = rotation, _onTap = onTap;

  int _flex;
  Color _color;
  double _rotation;

  int get flex => _flex;
  Color get color => _color;
  double get rotation => _rotation;

  set rotation(double value) {
    if (this._rotation == value) return;
    _rotation = value;
    markNeedsPaint();
  }

  set flex(int value) {
    assert(value >= 0);
    if (value != _flex) {
      _flex = value;
      parentData.flex = flex;
      markParentNeedsLayout();
    }
  }


  set color(Color value) {
    if (value != _color)  {
      _color = value;
      markNeedsPaint();
    }
  }

  @override
  bool get isRepaintBoundary => true;

  @override
  void detach() {
    _tapGestureRecognizer.dispose();
    super.detach();
  }

  @override
  bool hitTestSelf(Offset position) {
    return size.contains(position);
  }

  @override
  void handleEvent(PointerEvent event, covariant BoxHitTestEntry entry) {
    assert(debugHandleEvent(event, entry));
    if (event is PointerDownEvent) {
      _tapGestureRecognizer.addPointer(event);
    }
  }

  @override
  ColumnRTWParentData get parentData {
    if (super.parentData == null) {
      return null;
    }
    assert(super.parentData is ColumnRTWParentData, "$CustomBox can only be a direct child of $ColumnRTW");
    return super.parentData as ColumnRTWParentData;
  }

  @override
  void attach(PipelineOwner owner) {
    super.attach(owner);
    parentData.flex = _flex;
    _tapGestureRecognizer = TapGestureRecognizer(debugOwner: this)..onTap = _onTap;
  }

  @override
  bool get sizedByParent => true;

  @override
  Size computeDryLayout(BoxConstraints constraints) {
    // seems like biggest not work??
    // strange.
    return constraints.biggest;
  }

  @override
  void paint(PaintingContext context, Offset offset) {
    final smallRectWidth = size.shortestSide / 2;

    final canvas = context.canvas;
    canvas.drawRect(offset & size, _painter..color = this.color);
    
    canvas.save();
    canvas.translate(offset.dx + size.width / 2, offset.dy + size.height / 2);
    canvas.rotate(rotation);
    canvas.drawRect(Rect.fromCenter(center: Offset.zero, width: smallRectWidth, height: smallRectWidth)
        , Paint()..style = PaintingStyle.stroke..strokeWidth = 5..color = Colors.deepPurpleAccent);
    canvas.restore();
    // canvas.rotation(pi / 4);
  }
}
