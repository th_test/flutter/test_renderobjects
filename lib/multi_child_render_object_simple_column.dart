// This is for test MultiChildRenderObject which have multiy child

import 'dart:math';

import 'package:flutter/material.dart';
import 'package:test_renderobjects/column_rtw.dart';
import 'package:test_renderobjects/custom_box.dart';

class PageTestMultiChildRenderObjectSimpleColumn extends StatefulWidget {
  @override
  _PageTestMultiChildRenderObjectSimpleColumnState createState() => _PageTestMultiChildRenderObjectSimpleColumnState();
}

class _PageTestMultiChildRenderObjectSimpleColumnState extends State<PageTestMultiChildRenderObjectSimpleColumn> with SingleTickerProviderStateMixin {
  AnimationController _controller;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(vsync: this, duration: Duration(seconds: 2));
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: ColumnRTW(
        alignment: ColumnRTWAlignment.center,
        children: [
          AnimatedBuilder(
            animation: _controller,
            builder: (context, child) {
              return CustomBox(
                onTap: () {
                    if (_controller.isAnimating) {
                      _controller.stop();
                    } else {
                      _controller.repeat();
                    }},
                color: Colors.red,
                flex: 2,
                rotation: _controller.value * 2 * pi,
              );
            },
          ),
          Text("1"),
          Text("2"),
          Text("3"),
          Text("4"),
          CustomBox(
            color: Colors.green,
            flex: 6,
          ),
        ],
      ),
    );
  }
}
