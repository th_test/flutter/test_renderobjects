import 'dart:ffi';

import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class ProgressBar extends LeafRenderObjectWidget {
  final Color dotColor;
  final Color thumbColor;
  final double thumbSize;

  ProgressBar({this.dotColor, this.thumbColor, this.thumbSize});

  RenderObject createRenderObject(Object context) {
    return RenderProgressBar(
        dotColor: this.dotColor,
        thumbColor: this.thumbColor,
        thumbSize: this.thumbSize);
  }

  /// when did you call, when the widget changed, but can reuse render object.
  @override
  void updateRenderObject(
      BuildContext context, covariant RenderProgressBar renderObject) {
    renderObject
      ..dotColor = this.dotColor
      ..thumbSize = this.thumbSize
      ..thumbColor = this.thumbColor;
  }
}

class RenderProgressBar extends RenderBox {
  Color _dotColor;
  Color _thumbColor;
  double _thumbSize;

  RenderProgressBar({Color dotColor, Color thumbColor, double thumbSize})
      : _dotColor = dotColor,
        _thumbColor = thumbColor,
        _thumbSize = thumbSize;

  Color get dotColor => _dotColor;
  Color get thumbColor => _thumbColor;
  double get thumbSize => _thumbSize;

  double _currentThumbValue = 0.5;

  set dotColor(Color value) {
    if (value == _dotColor) return;
    _dotColor = value;
    markNeedsPaint();
  }

  set thumbColor(Color value) {
    if (value == _thumbColor) return;
    _thumbColor = value;
    markNeedsPaint();
  }

  set thumbSize(double value) {
    if (value == _thumbSize) return;
    _thumbSize = value;
    markNeedsLayout();
  }

  @override
  void performLayout() {
    final desireWidth = constraints.maxWidth;
    final desiredHeight = thumbSize;
    final desireSize = Size(desireWidth, desiredHeight);
    size = constraints.constrain(desireSize);
  }

  @override
  void paint(PaintingContext context, Offset offset) {
    final canvas = context.canvas;
    canvas.save();
    canvas.translate(offset.dx, offset.dy);
    
    canvas.drawRect(Rect.fromLTRB(0, 0, size.width, size.height), Paint()..color = Colors.black26);

    final dotPaint = Paint()
      ..color = dotColor
      ..strokeCap = StrokeCap.round
      ..strokeWidth = 4;

    final barPaint = Paint()
      ..color = Colors.red
      ..strokeCap = StrokeCap.round
      ..strokeWidth = 4;

    final spacing = size.width / 10;
    for (var i = 0; i < 11; i++) {
      var upperPoint = Offset(spacing * i, size.height * 0.75);
      var lowerPoint = Offset(spacing * i, size.height);

      if (i % 5 == 0) {
        upperPoint = Offset(spacing * i, size.height * 0.25);
      }

      canvas.drawLine(upperPoint, lowerPoint, dotPaint);
    }

    final thumbPaint = Paint()
    ..color = thumbColor;
    final thumbDx = _currentThumbValue * size.width;

    final point1 = Offset(0, size.height / 2);
    final point2 = Offset(thumbDx, size.height / 2);
    canvas.drawLine(point1, point2, barPaint);

    // paint thumb
    final center = Offset(thumbDx, size.height / 2);
    canvas.drawCircle(center, thumbSize / 2, thumbPaint);
    canvas.restore();
  }

  HorizontalDragGestureRecognizer _drag;

  @override
  void attach(covariant PipelineOwner owner) {
    super.attach(owner);
    _drag = HorizontalDragGestureRecognizer()
    ..onStart = (details) {_updateThumbPosition(details.localPosition);}
    ..onUpdate = (details) {_updateThumbPosition(details.localPosition);};
  }

  @override
  void detach() {
    _drag.dispose();
    super.detach();
  }

  @override
  bool hitTestSelf(Offset position) {
    return true;
  }

  @override
  bool handleEvent(PointerEvent event, BoxHitTestEntry entry) {
    assert(debugHandleEvent(event, entry));
    if (event is PointerDownEvent) {
      _drag.addPointer(event);
    }
  }

  void _updateThumbPosition(Offset localPosition) {
    var dx = localPosition.dx.clamp(0, size.width);
    _currentThumbValue = double.parse((dx / size.width).toStringAsFixed(1));
    markNeedsPaint();
    markNeedsSemanticsUpdate();
  }
}
