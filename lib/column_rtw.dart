import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';

enum ColumnRTWAlignment {
  start,
  center,
}

class ColumnRTW extends MultiChildRenderObjectWidget {
  final ColumnRTWAlignment alignment;

  ColumnRTW(
  {
    Key key,
    List<Widget> children = const [],
    this.alignment = ColumnRTWAlignment.start
  }): super(key: key, children: children);

  @override
  RenderObject createRenderObject(BuildContext context) {
    return RenderColumnRTW(alignment: this.alignment);
  }

  @override
  void updateRenderObject(BuildContext context, covariant RenderColumnRTW renderObject) {
    renderObject..alignment = this.alignment;
  }

}

class ColumnRTWParentData extends ContainerBoxParentData<RenderBox> {
  int flex = 0;
}

class RenderColumnRTW extends RenderBox with ContainerRenderObjectMixin<RenderBox, ColumnRTWParentData>, RenderBoxContainerDefaultsMixin<RenderBox, ColumnRTWParentData> {
  ColumnRTWAlignment _alignment;

  RenderColumnRTW({ColumnRTWAlignment alignment}): _alignment = alignment;

  ColumnRTWAlignment get alignment => _alignment;


  set alignment(ColumnRTWAlignment value) {
    if (_alignment == value) return;
    _alignment = value;
    this.markNeedsLayout();
  }

  @override
  double computeMaxIntrinsicHeight(double width) {
    double height = 0;
    var child = firstChild;
    while (child != null) {
      height += child.computeMaxIntrinsicHeight(width);
      child = (child.parentData as ColumnRTWParentData).nextSibling;
    }
    return height;
  }

  @override
  double computeMinIntrinsicHeight(double width) {
    double height = 0;
    var child = firstChild;
    while (child != null) {
      height += child.computeMinIntrinsicHeight(width);
      child = (child.parentData as ColumnRTWParentData).nextSibling;
    }
    return height;
  }

  @override
  void setupParentData(covariant RenderObject child) {
    if (child.parentData is! ColumnRTWParentData) {
      // I think parent data is the data that child pass to parent.
      child.parentData = ColumnRTWParentData();
    }
  }

  Size _getLayout(BoxConstraints constraints, {bool dry = true}) {
    var child = firstChild;
    var width = 0.0;
    var height = 0.0;

    var totalFlex = 0;
    RenderBox lastFlexObject;

    while (child != null) {
      var parentData = child.parentData as ColumnRTWParentData;
      if (parentData.flex != null && parentData.flex > 0) {
        totalFlex += parentData.flex;
        lastFlexObject = child;
      } else {
        // parentUsersSize can relayout when child size changed.
        Size childSize;
        var childConstraint = BoxConstraints(maxWidth: constraints.maxHeight);

        if (dry) {
          childSize = child.getDryLayout(childConstraint);
        } else {
          child.layout(childConstraint, parentUsesSize: true);
          childSize = child.size;
        }
        height += childSize.height;
        width = max(width, childSize.width);

      }
      child = parentData.nextSibling;
    }

    if (totalFlex > 0) {
      child = lastFlexObject;
      var perFlexHeight = (constraints.maxHeight - height) / totalFlex;
      while (child != null) {
        var parentData = child.parentData as ColumnRTWParentData;
        if (parentData.flex != null && parentData.flex > 0) {
          Size childSize;
          var flexHeight = perFlexHeight * parentData.flex;
          var childConstraints =  BoxConstraints(maxWidth: constraints.maxWidth, minHeight: flexHeight, maxHeight: flexHeight);

          if (dry)  {
            childSize = child.getDryLayout(childConstraints);
          } else {
            child.layout(childConstraints, parentUsesSize: true);
            childSize = child.size;
          }
          height += childSize.height;
          width = max(width, childSize.width);
        }
        child = parentData.previousSibling;
      }
    }
    return Size(width, height);
  }

  @override
  Size computeDryLayout(BoxConstraints constraints) {
    return _getLayout(constraints, dry: true);
  }

  // preformLayout is called by engine.
  @override
  void performLayout() {
    size = _getLayout(constraints, dry: false);

    RenderBox child = firstChild;
    var offset = Offset(0, 0);
    while (child != null) {
      var parentData = child.parentData as ColumnRTWParentData;
      var x = 0.0;
      if (this._alignment == ColumnRTWAlignment.center) {
        x = (this.size.width - child.size.width) / 2;
      }
      parentData.offset = Offset(x, offset.dy);
      offset += Offset(0, child.size.height);

      child = parentData.nextSibling;
    }

  }

  @override
  bool hitTestChildren(BoxHitTestResult result, {Offset position}) {
    return defaultHitTestChildren(result, position: position);
  }

  @override
  void paint(PaintingContext context, Offset offset) {
    defaultPaint(context, offset);
  }
}

class CustomExpanded extends ParentDataWidget<ColumnRTWParentData> {
  const CustomExpanded({Key key, this.flex = 1, Widget child}): assert(flex > 0), super(key: key, child: child);

  final int flex;

  @override
  void applyParentData(RenderObject renderObject) {
    final parentData = renderObject.parentData as ColumnRTWParentData;
    if (parentData.flex != flex) {
      parentData.flex = flex;

      final targetObject = renderObject.parent;
      if (targetObject is RenderObject) {
        targetObject.markNeedsLayout();
      }
    }
  }

  @override
  Type get debugTypicalAncestorWidgetClass => CustomExpanded;
}